/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_3;

/**
 *
 * @author Mostafa Rafea
 */
public class Assignment_3 {

    /**
     * @param args the command line arguments
     */
    
    public static void heapify(int[] arr, int size, int index) {
        
        // index is refering to first non-leaf node
        // arr is refering to the two heaps after merging
        
        if (index >= size) { 
            return; 
        } 
        
        int leftChild = 2 * index + 1;
        int rightChild = 2 * index + 2;
        int max;
        
        if (leftChild < size && arr[leftChild] > arr[index]) {
            max = leftChild;
        }
        else {
            max = index;
        }
        if (rightChild < size && arr[rightChild] > arr[max]) {
            max = rightChild;
        }
        
        if (max != index) {
            int temp = arr[max];
            arr[max] = arr[index];
            arr[index] = temp;
            heapify(arr, size, max);
        }
        
    }
    
    public static void merge (int[] mergedHeap, int[] h1, int[] h2, int s1, int s2) {
        
        for (int i = 0; i < s1; i++) {
            mergedHeap[i] = h1[i];
        }
        
        for (int i = 0; i < s2; i++) {
            mergedHeap[i + s1] = h2[i];
        }
        
        int size = s1 + s2;
        
        for (int i = (size/2 - 1); i >= 0; i--) {
            heapify(mergedHeap, size, i);
        }
        
    }
    
    public static void main(String[] args) {
        int[] a = {7, 15, 35, 3};
        int[] b = {8, 9, 13};
        int n = a.length;
        int m = b.length;

        int[] merged = new int[m + n];

        merge(merged, a, b, n, m);

        for (int i = 0; i < m + n; i++) {
            System.out.print(merged[i] + " ");
        }
        System.out.println();
    }
    
}
